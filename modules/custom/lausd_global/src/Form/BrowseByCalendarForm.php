<?php

namespace Drupal\lausd_global\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;

/**
 *
 */
class BrowseByCalendarForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   */
  public function getFormId() {
    return 'browse_by_calendar_form';
  }

  /**
   * Form constructor.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /*$MonthArray = [];
    for($i=1; $i<=12; $i++){
    $month  =  date('F',strtotime("last day of - $i month"));
    $MonthArray[$i]=$month;
    }*/

    $current_url = Url::fromRoute('<current>');
    $path = $current_url->toString();
    // OR of you want to get the url without language prefix.
    $path = $current_url->getInternalPath();
    $path_args = explode('/', $path);
    $arg0 = '';
	if (!empty($path_args[1])) {
      $defaultmonth = substr($path_args[1], -2);
      $defaultyear = substr($path_args[1], 0, 4);
      $arg0 = $path_args[1];
    }
    else {
      $defaultmonth = date('m');
      $defaultyear = date('Y');
    }
    $form['#prefix'] = '<div id="calender_month_year_select_form" class="calender_month_year_select_form">';
    $form['#suffix'] = '</div>';
    $MonthArray = [
      "01" => "January",
      "02" => "February",
      "03" => "March",
      "04" => "April",
      "05" => "May",
      "06" => "June",
      "07" => "July",
      "08" => "August",
      "09" => "September",
      "10" => "October",
      "11" => "November",
      "12" => "December",
    ];

    $yearOption = [];
    for ($i = 0; $i <= 10; $i++) {
      $yearOption[date('Y', strtotime("+ $i year"))] = date('Y', strtotime("+ $i year"));
    }
    $sort_options = ['created' => $this->t('Date'), 'title' => $this->t('A-Z')];
    $form['browseby'] = [
      '#type' => 'markup',
      '#markup' => 'BROWSE BY',
      '#prefix' => '<div class="tw-flex select-list-row"><div class="tw-w-1/4 browse-by select-list-label"><span>',
      '#suffix' => '</span>',
      '#attributes' => ['class' => ['tw-w-1/4']],
    ];
    $form['calendar_link'] = [
      '#title' => $this->t('<i class="fa fa-calendar"></i>'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.content_field_date_on_calendar.page_month', ['arg_0' => $arg0]),
      '#attributes' => ['class' => [stristr($path, 'calendar-list') ? '' : 'active']],
      '#prefix' => '<span class="calendar-button">',
    ];
    $form['list_link'] = [
      '#title' => $this->t('<i class="fa fa-list"></i>'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.calendar_list.page_1', ['arg_0' => $arg0]),
      '#attributes' => ['class' => [stristr($path, 'calendar-list') ? 'active' : '']],
      '#suffix' => '</span></div>',
    ];
	$form['current_view'] = [
      '#type' => 'hidden',
      '#value' => stristr($path, 'calendar-list') ? 'view.calendar_list.page_1' : 'view.content_field_date_on_calendar.page_month',
    ];
    $form['date'] = [
      '#type' => 'markup',
      '#markup' => '<span class="label-text">DATE</span>',
      '#prefix' => '<div class="tw-w-1/4 date-filter tw-flex select-list-label">',
    ];
    $form['month'] = [
      '#type' => 'select',
      '#options' => $MonthArray,
      '#default_value' => $defaultmonth,
      '#empty_option' => $this->t('- Month - '),
      '#attributes' => ['class' => ['month-select ']],
      '#ajax' => [
        'callback' => '::calendermonthCallback',
        'wrapper' => 'calender_month_year_select_form',
      ],
      '#prefix' => '<div class="calendar-form-action-wrapper"><div class="calendar-select-list-drop-down">',
      '#suffix' => '</div>',
    ];
    $form['year'] = [
      '#type' => 'select',
      '#options' => $yearOption,
      '#default_value' => $defaultyear,
      '#empty_option' => $this->t('- year - '),
      '#attributes' => ['class' => ['month-select']],
      '#attributes' => ['class' => ['year-select']],
      '#ajax' => [
        'callback' => '::calenderyearCallback',
        'wrapper' => 'calender_month_year_select_form',
      ],
      '#prefix' => '<div class="calendar-select-list-drop-down">',
      '#suffix' => '</div></div></div>',
    ];

    $form['print'] = [
      '#type' => 'markup',
      '#markup' => '<span class="label-text">Print</span>',
      '#prefix' => '<div class="tw-w-1/2 print-button select-list-label print-section">'

    ];
    $form['board_meeting_schedule'] = [
      '#title' => $this->t('2019-20 Board Meeting Schedule'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.content_field_date_on_calendar.page_month', ['arg_0' => $arg0]),
      '#prefix' => '<div class="print-section-download-button">'
    ];
    $form['committee_meeting_schedule'] = [
      '#title' => $this->t('2019-20 Committee Meeting Schedule'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.calendar_list.page_1', ['arg_0' => $arg0]),
      '#suffix' => '</div></div></div>',
    ];
    return $form;
  }

  /**
   * Form submission handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Month Submit Callback handler.
   */
  public function calendermonthCallback(array &$form, FormStateInterface $form_state) {
    $yearvalue = $form_state->getValue('year');
    $monthvalue = $form_state->getValue('month');
    $current_view = $form_state->getValue('current_view');

    if (!empty($yearvalue)) {
      $year = $yearvalue;
    }
    else {
      $year = date('Y');
    }

    if (!empty($monthvalue)) {
      $month = $monthvalue;
    }
    else {
      $month = date('m');
    }

    $url = Url::fromRoute($current_view, ['arg_0' => $yearvalue.$monthvalue])->toString();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url));
    return $response;

  }

  /**
   * Year Submit Callback handler.
   */
  public function calenderyearCallback(array &$form, FormStateInterface $form_state) {
    $yearvalue = $form_state->getValue('year');
    $monthvalue = $form_state->getValue('month');
    $current_view = $form_state->getValue('current_view');

    if (!empty($yearvalue)) {
      $year = $yearvalue;
    }
    else {
      $year = date('Y');
    }

    if (!empty($monthvalue)) {
      $month = $monthvalue;
    }
    else {
      $month = date('m');
    }

	$url = Url::fromRoute($current_view, ['arg_0' => $yearvalue.$monthvalue])->toString();
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url));
    return $response;

  }

}
