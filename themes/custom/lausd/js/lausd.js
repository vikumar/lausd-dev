(function ($, Drupal) {
   Drupal.behaviors.lausd = {
      attach: function (context, settings) {

         $('.page-node-type-meeting #prevnext-previous a').text('Previous Meeting');
         $('.page-node-type-meeting #prevnext-next a').text('Next Meeting');
         $('.newsletter-area .simplenews-subscriber-form .form-item-message').insertAfter('.newsletter-area .simplenews-subscriber-form h2');

         $('.simplenews-subscriber-form .form-email').attr("placeholder", "Enter Your Email Address");
         $('.calender_month_year_select_form').parents('body').addClass('month_year_select_form');

         $('#maindiv div[data-drupal-messages]').addClass('status-message');
         $('#maindiv .status-message').children('div').addClass('status-info');

         $('.wrapper-meeting-section .right-link-section a, .agenda-live-stream a, .views-field-field-website-url a').click(function () {
            $(this).attr('target', '_blank');
         });

         var statuslength = $('#maindiv .status-message .status-info').length;
         if (statuslength < 2) {
            jQuery('#maindiv .status-message').remove();
         }

         function windowSize() {

            var width = $(window).width();

            if (width >= 768 && width <= 1023) {

               $(".site-header .header-bottom .menu-area ul li a.faq-item:contains('Frequently Asked Questions')").each(function () {
                  var newText = $(this).html().replace("Frequently Asked Questions", "FAQ");
                  var indicator = '<span class="sf-sub-indicator"> �</span>';
                  var replacetext = newText + indicator;
                  $(this).html(replacetext);
               });

            } else {
               var indicator = '<span class="sf-sub-indicator"> �</span>';
               $(".site-header .header-bottom .menu-area ul li a.faq-item").html('Frequently Asked Questions' + indicator);
            }

            if (width <= 767) {
               /*******************NextPrev Nav  **************/
               $("#nextprev-navigation #month-meeting").insertBefore("#nextprev-navigation #prevnext-previous");

               /**************** js for calander mobile ************/
               $(".calender_month_year_select_form .select-list-label.print-button").insertBefore(".calender_month_year_select_form .select-list-label.date-filter");
               $('.calender_month_year_select_form .print-button .label-text').text('Print Meeting Schedules');
               $('.calender_month_year_select_form .date-filter .label-text').text('Browse By');
            } else {
               /*******************NextPrev Nav  **************/
               $("#nextprev-navigation #month-meeting").insertAfter("#nextprev-navigation #prevnext-previous");

               /**************** js for calander mobile ************/
               $(".calender_month_year_select_form .select-list-label.print-button").insertAfter(".calender_month_year_select_form .select-list-label.date-filter");
               $('.calender_month_year_select_form .print-button .label-text').text('Print');
               $('.calender_month_year_select_form .date-filter .label-text').text('Date');
            }

            if (width < 582) {
               /*******************NextPrev Nav next trim  **************/
               var next_btn_val = $("#nextprev-navigation #prevnext-next a").text();
               if (next_btn_val.length > 27)
                  $("#nextprev-navigation #prevnext-next a").text(next_btn_val.substring(0, 27) + "…");
            }
            if (width < 400) {
               /*******************NextPrev Nav next trim  **************/
               var next_btn_val = $("#nextprev-navigation #prevnext-next a").text();
               if (next_btn_val.length > 13)
                  $("#nextprev-navigation #prevnext-next a").text(next_btn_val.substring(0, 13) + "…");
            }

         }
         windowSize();
         $(window).resize(function () {
            windowSize();
         });

      }
   };
})(jQuery, Drupal);


jQuery(document).ready(function () {

   /******************footer newletter ********** */
   jQuery('.newsletter-area #block-simplenewssubscription h2').each(function () {
      var newDiv = jQuery('<div/>').addClass('subscriber-form-message');
      jQuery(this).before(newDiv);
      var next = jQuery(this).next();
      newDiv.append(this).append(next);
   });

   /************************ Header Main Menu Hover ************/
   jQuery('.site-header-small .header-right').click(function () {
      jQuery('.site-header-small .header-right .menu-open').toggleClass('tw-opacity-0');
      jQuery('.site-header-small .header-right .menu-close').toggleClass('tw-opacity-100');
      jQuery('.site-header-small .header-right').toggleClass('tw-bg-blue-900');
      jQuery('.site-header-small .heading-bottom').toggleClass('tw-visible tw-opacity-100');
   });

   /*******************JS FOR acrchive page**********/
   var petitionsspan = jQuery(".charter-petitions-select-list-section .charter-petitions-select-list span");
   petitionsspan.replaceWith(function () {
      return jQuery('<option/>', {
         html: this.innerHTML
      });
   });
   jQuery(".charter-petitions-select-list-section .charter-petitions-select-list option").wrapAll("<select id='charter-petitions-select'></select>");
   jQuery('.charter-petitions-select-list-section #charter-petitions-select option').each(function () {
      var optionvalue = jQuery(this).find('a').text();
      jQuery(this).val(optionvalue);
   });
   jQuery('#charter-petitions-select').before('<label for="letter" class="cap mr-4">Jump to a School By Letter:</label>');

   jQuery('select#charter-petitions-select').on('change', function () {
      jQuery('html,body').animate({
         scrollTop: jQuery('h3.' + jQuery(this).val()).offset().top - 80
      }, 500);
   });

   jQuery('.charter-petitions-select-list-section .back-top-top').click(function () {

      if (jQuery('body').hasClass('toolbar-fixed')) {
         jQuery('html,body').animate({
            scrollTop: jQuery('#maindiv').offset().top - 85
         }, 500);
      } else {
         jQuery('html,body').animate({
            scrollTop: jQuery('#maindiv').offset().top
         }, 500);
      }

   });

   /**************************************Sticky Content header charter-petitions */
   var parent = jQuery(".charter-petitions-select-list-section .view-row").each(function () {
      this._child = jQuery(this).find("#sticky-header");
   });
   function fixpos() {
      parent.each(function () {
         var br = this.getBoundingClientRect();
         if (jQuery(window).width() >= 768)
            jQuery(this._child).toggleClass("sticky", br.top < 100 && br.bottom > 60);
         else
            jQuery(this._child).toggleClass("sticky", br.top < 55 && br.bottom > 50);

         jQuery('.path-charterpetitions .charter-petitions-select-list-section h3.sticky').each(function () {
            jQuery(this).width(jQuery(this).parent().width());
         });
      });
   }
   fixpos();
   jQuery(window).on("load scroll", fixpos);
   /****************************** */

   jQuery('.charter-petitions-select-list-section').parents('body').addClass('charter-petitions-list-wrap');
   if (jQuery('body').hasClass('charter-petitions-list-wrap')) {
      var breadcrum_team = "<div class='breadcrumb charter-petitions-breadcrumb'><ol class='tw-flex tw-flex-wrap tw-my-8'><li class='flex items-center cap'><a href='document-library'>Document Library</a><span class='seprater'></span></li><li class='flex items-center cap'>Charter Petition Archive</li></ol></div>";
      jQuery('body.charter-petitions-list-wrap #block-lausd-page-title').before(breadcrum_team);
   }

   jQuery('.meet-the-team-listing').parents('body').addClass('meet-the-team-listing-wrap');
   if (jQuery('body').hasClass('meet-the-team-listing-wrap')) {
      var breadcrum_team = "<div class='breadcrumb meet-team-breadcrumb'><ol class='tw-flex tw-flex-wrap tw-my-8'><li class='flex items-center cap'><a href='document-library'>Document Library</a><span class='seprater'></span></li><li class='flex items-center cap'>Meet The Team</li></ol></div>";
      jQuery('body.meet-the-team-listing-wrap #block-lausd-page-title').before(breadcrum_team);
   }
   jQuery('.charterpetitions-details-breadcrumb').parents('body').addClass('charterpetitions-breadcrumb-wrap');
   if (jQuery('body').hasClass('charterpetitions-breadcrumb-wrap')) {
      jQuery('body.charterpetitions-breadcrumb-wrap #block-lausd-page-title').insertAfter('.charterpetitions-details-breadcrumb');
   }

   jQuery('.breadcrumb').parents('body').addClass('breadcrumb-nospace');

   /*******************JS FOR acrchive page end**********/


   /******************* calendar page reload   **********/
   jQuery(window).on('load resize', function(){ //load list page if view or resize for mobile screen
      var width = jQuery(this).width();
      if (width < 1024) {
         if( (window.location.pathname).substring((window.location.pathname.lastIndexOf("/"))) == "/calendar")
            location.replace("lausd/calendar-list");
         else if( (window.location.pathname).includes("/calendar/") ){
            var cal_month = (window.location.pathname).substring((window.location.pathname.lastIndexOf("/")));
            location.replace("/calendar-list"+cal_month);
         }
      }
   });
   flag_screen_pc = false
   if(jQuery(window).width() >= 1024 )
      flag_screen_pc = true;
   jQuery(window).on('resize', function(){      //load calendar page if resize for pc screen
      var width = jQuery(this).width();
      if (width >= 1024 && !flag_screen_pc ){
         if( (window.location.pathname).substring((window.location.pathname.lastIndexOf("/"))) == "/calendar-list")
            location.replace("/calendar");
         else if( (window.location.pathname).includes("/calendar-list/") ){
            var cal_month = (window.location.pathname).substring((window.location.pathname.lastIndexOf("/")));
            location.replace("/calendar"+cal_month);
         }
      }
   });
});
